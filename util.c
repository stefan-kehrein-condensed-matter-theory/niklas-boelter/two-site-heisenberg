/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#include "util.h"

extern inline int min(int a, int b);
extern inline int max(int a, int b);

extern inline uint32_t fallback_pext_u32(uint32_t value, uint32_t mask);
extern inline uint32_t fallback_pdep_u32(uint32_t value, uint32_t mask);

clock_t start;
int argv_len; // Used for status reporting
char *argv_ptr;
char *status_prefix; // Prefix string for status reporting


void show_rho_eigenvalues(double *rho_eigenvalues, int dim)
{
    int i;
    double trace;

    trace = 0.0;
    for(i = 0; i < dim; i++)
    {
        trace += rho_eigenvalues[i];
    }
    printf("Trace: %g\n  ", trace);

    printf("Eigenvalues: ");
    for(i = 0; i < dim; i++)
    {
        if(fabs(rho_eigenvalues[i]) > 1E-12)
        {
            printf("%g ", rho_eigenvalues[i]);
        }
    }
    printf("\n");
}


double calculate_Renyi_entropy(double *eigenvalues, int M, double alpha)
{
    int i;
    double sum = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum += pow(eigenvalues[i], alpha);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "calculate_Renyi_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    return log2(sum)/(1-alpha);
}


double calculate_vonNeumann_entropy(double *eigenvalues, int M)
{
    int i;
    double SvN = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            SvN = SvN - eigenvalues[i]*log2(eigenvalues[i]);
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
       exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_vonNeumann_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
       exit(EX_SOFTWARE);
    }
    return SvN;
}


double calculate_min_entropy(double *eigenvalues, int M)
{
    int i;
    double max_eigenvalue = 0.0;
    double sum_positive = 0.0;
    double sum_negative = 0.0;
    for(i = 0; i < M; i++)
    {
        if (eigenvalues[i] > 0.0)
        {
            sum_positive += eigenvalues[i];
        }
        else
        {
            sum_negative -= eigenvalues[i];
        }

        if(eigenvalues[i] > max_eigenvalue)
        {
            max_eigenvalue = eigenvalues[i];
        }
    }
    if(fabs(sum_positive - 1.0) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Sum of positive Eigenvalues != 1\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    if(fabs(sum_negative) > 1E-6)
    {
        fprintf(stderr, "\ncalculate_min_entropy: Negative Eigenvalues\n");
        show_rho_eigenvalues(eigenvalues, M);
        exit(EX_SOFTWARE);
    }
    return -1.0 * log2(max_eigenvalue);
}


void diagonalize(double _Complex *M, int dim, double _Complex *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;
    int dvr = 1;

    start = clock();

    rwork = malloc(2 * dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, &workopt, &lwork, rwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zgeev_("None", "None", &dim, M, &dim, eigenvalues, 
        NULL, &dvr, NULL, &dvr, work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(EX_SOFTWARE);
    }
    // report_elapsed_time();
}


void diagonalize_hermitian(double _Complex *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    rwork = malloc(3 * dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("None", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric_eigenvectors(double *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    lwork = -1;
    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_hermitian_eigenvectors(double _Complex *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double _Complex workopt;
    double _Complex *work;
    double *rwork;

    start = clock();

    rwork = malloc(3 * dim * sizeof(double));

    if(!rwork)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    lwork = -1;
    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        &workopt, &lwork, rwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zheev_("Vectors", "Upper", &dim, M, &dim, eigenvalues, 
        work, &lwork, rwork, &ret);

    free(rwork);
    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(EX_SOFTWARE);
    }
    report_elapsed_time();
}


void diagonalize_symmetric(double *M, int dim, double *eigenvalues)
{
    /* Lapacke variables */
    int ret, lwork;
    double workopt;
    double *work;

    start = clock();

    lwork = -1;
    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, &workopt, &lwork, &ret);

    lwork = (int) workopt;

    work = malloc(lwork * sizeof(double));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    dsyev_("None", "Upper", &dim, M, &dim, eigenvalues, work, &lwork, &ret);

    free(work);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR Diagonalization failed.\n");
        exit(EX_SOFTWARE);
    }
    
    report_elapsed_time();
}


void report_status(const char *status)
{
    char buf[2048];

    memset(buf, '\0', 2048);

    if(status_prefix)
    {
        snprintf(buf, sizeof(buf), "%s%s", status_prefix, status);
    }
    else
    {
        snprintf(buf, sizeof(buf), "%s", status);
    }

    if(argv_ptr)
    {
        memset(argv_ptr, '\0', argv_len);
        snprintf(argv_ptr, argv_len, "%s", buf);
    }
    printf("\r%s", buf);
    fflush(stdout);
}


void report_elapsed_time()
{
    clock_t stop = clock();
    long ms = (stop - start) * 1000 / CLOCKS_PER_SEC;

    if(ms > 1000*60*60)
    {
        printf("# Time taken: %4ld hours %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)/60, ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else if(ms > 1000*60)
    {
        printf("# Time taken: %2ld minutes %2ld seconds %4ld milliseconds\n",
        ((ms/1000)/60)%60, (ms/1000)%60, ms%1000);
    }
    else
    {
        printf("# Time taken: %2ld seconds %4ld milliseconds\n",
        (ms/1000)%60, ms%1000);
    }
    
}


void check_normalized(const double *V, const int64_t N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += V[i] * V[i];
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized!\n");
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_normalized_complex(const double _Complex *V, const int64_t N)
{
    int i;
    double squaredNorm = 0.0;
    for(i = 0; i < N; i++)
    {
        squaredNorm += creal(V[i] * conj(V[i]));
    }

    if(fabs(squaredNorm - 1) > 1E-12)
    {
        fprintf(stderr, "\nError: V is not normalized! %g \n", squaredNorm);
        exit(EX_SOFTWARE);
    }
    // printf("\nNormalized ✔︎\n");
}


void check_unitary(const double _Complex *U, const int64_t N)
{
    int i,j,k;
    double _Complex matrixElement;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            matrixElement = 0.0 + 0.0 * _Complex_I;
            for(k = 0; k < N; k++)
            {
                matrixElement += U[i + N * k] * conj(U[j + N * k]);
            }

            if((i == j && cabs(matrixElement - 1.0) > 1E-12) || 
               (i != j && cabs(matrixElement) > 1E-12))
            {
                fprintf(stderr, "\nError: O is not unitary!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nUnitary ✔︎\n");
}


void check_hermitian(const double _Complex *rho, const int64_t N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(cabs(rho[i + N * j] - conj(rho[j + N * i])) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not hermitian!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nHermitian ✔︎\n");
}


void check_symmetric(const double *rho, const int64_t N)
{
    int i,j;

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            if(fabs(rho[i + N * j] - rho[j + N * i]) > 1E-12)
            {
                fprintf(stderr, "\nError: rho is not symmetric!\n");
                exit(EX_SOFTWARE);
            }
        }
    }
    printf("\nSymmetric ✔︎\n");
}
