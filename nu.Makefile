CC=clang
CFLAGS=-O2 -std=c99 -Weverything -Wno-sign-conversion -Wno-double-promotion -Wno-reserved-id-macro -Wno-padded -isystem/usr/local/opt/lapack/include -isystem/usr/include/lapacke/ -march=native
LIBS=-lm -llapack -lsqlite3 -L/usr/local/opt/lapack/lib
OBJECTS=util.o
EXECUTABLES=two-site-heisenberg-tfd random-unitary-two-site-heisenberg-tfd

default: $(EXECUTABLES)

two-site-heisenberg-tfd: two-site-heisenberg-tfd.c $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

random-unitary-two-site-heisenberg-tfd: two-site-heisenberg-tfd.c random-unitary.c $(OBJECTS)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS) -DHAAR_RANDOM_UNITARY

clean:
	rm -f $(OBJECTS) $(EXECUTABLES)