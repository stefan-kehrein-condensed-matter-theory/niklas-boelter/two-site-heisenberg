#!/usr/bin/env python3
# -*- coding: utf8 -*-
import sqlite3
import numpy
import itertools
import os
from os.path import expanduser

import matplotlib.pyplot as pyplot


home = expanduser("~")
conn = sqlite3.connect(home + '/two-site-heisenberg/two-site-heisenberg.db')
c = conn.cursor()


# J > 0: Ferromagnet
# J < 0: Antiferromagnet

def plot_figure(subplot_index, J_x, J_y, J_z, B, beta):
	pyplot.subplot(subplot_index)
	os.system(home + '/two-site-heisenberg/random-unitary-two-site-heisenberg-tfd {} {} {} 0.0 0.0 0 1 10000 > /dev/null'.format(J_x, J_y, J_z))
	os.system(home + '/two-site-heisenberg/two-site-heisenberg-tfd {} {} {} {} {} 0 0.005 1.57 > /dev/null'.format(J_x, J_y, J_z, B, beta))

	data_Haar = list(c.execute("SELECT AVG(I3), AVG(I3_2), AVG(I3_min) FROM results_Haar_nosym " +
	        "WHERE J_x = ? AND J_y = ? AND J_z = ? AND beta = 0 AND B = 0", (J_x, J_y, J_z)))[0]

	data = numpy.array(list(zip(
	    *(c.execute(
	        "SELECT t, I3, I3_2, I3_min FROM results " +
	        "WHERE J_x = ? AND J_y = ? AND J_z = ? AND beta = ? AND B = ?", (J_x, J_y, J_z, beta, B))))))

	pyplot.plot(data[0]/numpy.pi, data[1]/data_Haar[0], '-', label=r"$I_3$")
	pyplot.plot(data[0]/numpy.pi, data[2]/data_Haar[1], '-', label=r"$I_3^{(2)}$")
	pyplot.plot([0,0.5],[1,1], linewidth=0.4, color='black')
	# pyplot.plot(data[0]/numpy.pi, data[3]/data_Haar[2], '-', label=r"$I_3^\mathrm{min}$")
	pyplot.xlabel(r"$t/\pi$")
	pyplot.ylabel(r"$I_3/I_3^H$")
	pyplot.title(r"$\vec J = ({}, {}, {}), B_z = {}, \beta = {}$".format(J_x, J_y, J_z, B, beta))
	pyplot.legend()

pyplot.figure(figsize=(12,8))
pyplot.subplots_adjust(hspace=0.35, wspace=0.25, left=0.08, right=0.95, top=0.95, bottom=0.05 )

B = 0

plot_figure(321,  1,  1,  1, B, 0.0)
plot_figure(322, -1, -1, -1, B, 0.0)
plot_figure(323,  1,  1,  1, B, 0.5)
plot_figure(324, -1, -1, -1, B, 0.5)
plot_figure(325,  1,  1,  1, B, 2)
plot_figure(326, -1, -1, -1, B, 2)

pyplot.show()
