/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#ifndef RANDOM_UNITARY_H
#define RANDOM_UNITARY_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <sysexits.h>
#include <complex.h>

#include <sys/types.h>
#include <sys/stat.h>

#ifdef USE_INTEL_MKL
#define MKL_Complex16 double _Complex
#include <mkl.h>
#else
#include <lapacke.h>
#endif

#ifdef __linux__ 
#include <bsd/stdlib.h>
#endif

void get_Haar_random_unitary(double _Complex *U, int32_t N);
void get_diagonal_unitary(double _Complex *U, int N);
void get_identity_unitary(double _Complex *U, int N);

#endif
