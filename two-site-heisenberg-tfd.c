/*  Copyright 2019 Niklas Bölter <niklas.boelter@theorie.physik.uni-goettingen.de> */

#include <assert.h>
#include <sqlite3.h>
#include "util.h"

#ifdef HAAR_RANDOM_UNITARY
#include "random-unitary.h"
#define TABLENAME "results_Haar_nosym"
#else
#define TABLENAME "results"
#endif

#define DBPATH "two-site-heisenberg.db"

static double beta = 0.0;
static double dt = 1.0;
static double starttime = 0.0;
static double stoptime  = 100.0;
static double J_x;
static double J_y;
static double J_z;
static double B = 0.0;

static double *hamiltonian;
static double *eigenvalues;
static double _Complex *tfd_state;

#ifdef HAAR_RANDOM_UNITARY
static double _Complex *random_unitary;
static double _Complex *rotated_eigenvectors;
#endif

static double *rho_AB;
static double *rho_AB_eigenvalues;

static double *rho_A;
static double *rho_A_eigenvalues;

static double _Complex *rho_AX;
static double *rho_AX_eigenvalues;

static double t;
static double S_A, S_AB, S_AC, S_AD;
static double S2_A, S2_AB, S2_AC, S2_AD;
static double Smin_A, Smin_AB, Smin_AC, Smin_AD;
static double I3, I3_2, I3_min;

// Sqlite3 
static sqlite3 *db;
static int rc;
static char *zErrMsg = 0;
static sqlite3_stmt *stmt_select;
static sqlite3_stmt *stmt_insert;

void open_database(void);
int  find_results(void);
void store_results(void);
void close_database(void);
void usage(char*);

void usage(char *argv0)
{
    printf("Usage: %s J_x J_y J_z B beta start dt stop\n\n", argv0);
    printf("Calculates tripartite information of TFD state\n");
    printf("System: two-site Heisenberg model\n\n");
    printf("H = -(J_x σ_x⊗σ_x + J_y σ_y⊗σ_y + J_z σ_z⊗σ_z + B (1⊗σ_z+σ_z⊗1) ) \n");
    printf("J_x\tCoupling constant in x-direction\n");
    printf("J_y\tCoupling constant in x-direction\n");
    printf("J_z\tCoupling constant in x-direction\n");
    printf("B\tMagnetic field in z-direction\n");
    printf("beta\t Inverse Temperature\n");
    printf("start\t Start time\n");
    printf("dt\t Time difference between sampling points\n");
    printf("stop\t Stop time\n");
}

int main(int argc, char *argv[])
{
    clock_t init;
    char buf[1024];
    int i,j,k,step;
    int A_row, A_column, B_diag, C_diag, C_row, C_column, D_diag, D_row, D_column;
    int dim = 4;
    const double sign[4] = {1.0, -1.0, -1.0, 1.0};
    
    init = clock();

    if(argc != 9)
    {
        usage(argv[0]);
        exit(EX_USAGE);
    }

    J_x = atof(argv[1]);
    J_y = atof(argv[2]);
    J_z = atof(argv[3]);
    B = atof(argv[4]);
    beta = atof(argv[5]);
    starttime = atof(argv[6]);
    dt = atof(argv[7]);
    stoptime = atof(argv[8]);

    if(beta < 0.0)
    {
        fprintf(stderr, "The temperature must be positive.");
        exit(EX_USAGE);
    }
    if(dt <= 0)
    {
        fprintf(stderr, "Parameter dt must be positive.");
        exit(EX_USAGE);
    }

    hamiltonian = calloc(dim * dim, sizeof(double));
    eigenvalues = calloc(dim, sizeof(double));
    tfd_state = calloc(dim * dim, sizeof(double _Complex));

    rho_AB = calloc(dim * dim, sizeof(double));
    rho_AB_eigenvalues = calloc(dim, sizeof(double));

    rho_A = calloc(2 * 2, sizeof(double));
    rho_A_eigenvalues = calloc(2, sizeof(double));

    rho_AX = calloc(dim * dim, sizeof(double _Complex));
    rho_AX_eigenvalues = calloc(dim, sizeof(double));

    if(!hamiltonian || !eigenvalues || !tfd_state || !rho_AB || !rho_AB_eigenvalues )
    {
        fprintf(stderr, "ERROR: Could not allocate memory.\n");
        exit(EX_OSERR);
    }
    for(i = 0; i < dim; i++)
    {
        hamiltonian[i + dim * (3-i)] += -1.0 * J_x;
        hamiltonian[i + dim * (3-i)] += J_y * sign[i];
        hamiltonian[i + dim * i] += -1.0 * J_z * sign[i];
    }
    hamiltonian[0 + dim * 0] += -2*B;
    hamiltonian[3 + dim * 3] += 2*B;

    for(i = 0; i < dim; i++)
    {
        for(j = 0; j < dim; j++)
        {
            printf("%g  ", hamiltonian[i + dim * j]);
        }
        printf("\n");
    }
    printf("\n\n");

#ifdef HAAR_RANDOM_UNITARY
    random_unitary = calloc(dim * dim, sizeof(double _Complex));
    rotated_eigenvectors = calloc(dim * dim, sizeof(double _Complex));
    if(!random_unitary || !rotated_eigenvectors)
    {
        fprintf(stderr, "ERROR: Could not allocate memory.\n");
        exit(EX_OSERR);
    }
#endif
    

    check_symmetric(hamiltonian, dim);
    report_status("Diagonalizing Hamiltonian");
    diagonalize_symmetric_eigenvectors(hamiltonian, dim, eigenvalues);

    double min_eigenvalue = 0.0;

    for(k = 0; k < dim; k++)
    {
        if(eigenvalues[k] < min_eigenvalue)
        {
            min_eigenvalue = eigenvalues[k];
        }
    }

    for(k = 0; k < dim; k++)
    {
        eigenvalues[k] -= min_eigenvalue;
    }

    for(i = 0; i < dim; i++)
    {
        printf("E = %g\n", eigenvalues[i]);
        for(j = 0; j < dim; j++)
        {
            printf("%g+i%g  ", creal(hamiltonian[j + dim * i]), cimag(hamiltonian[j + dim * i]));
        }
        printf("\n");
    }
    printf("\n\n");

    double partitionSum = 0.0;
    for(k = 0; k < dim; k++)
    {
        partitionSum += exp(-1.0 * beta * eigenvalues[k]);
    }

    memset(rho_AB, 0, dim * dim * sizeof(double));
    for(k = 0; k < dim; k++)
    {
        for(i = 0; i < dim; i++)
        {
            for(j = 0; j < dim; j++)
            {
                rho_AB[i + dim * j] +=
                    exp(-1.0 * beta * eigenvalues[k])
                    * hamiltonian[i + dim * k]
                    * hamiltonian[j + dim * k]
                    / partitionSum;
            }
        }
    }

    check_symmetric(rho_AB, dim);

    memset(rho_A, 0, 2 * 2 * sizeof(double));
    for(A_row = 0; A_row < 2; A_row++)
    {
        for(A_column = 0; A_column < 2; A_column++)
        {
            for(B_diag = 0; B_diag < 2; B_diag++)
            {
                rho_A[A_row + (A_column<<1)] +=  rho_AB[A_row + ((B_diag + ((A_column + (B_diag<<1))<<1))<<1)];
            }
        }   
    }
    check_symmetric(rho_A, 2);

    diagonalize_symmetric(rho_AB, dim, rho_AB_eigenvalues);

    S_AB = calculate_vonNeumann_entropy(rho_AB_eigenvalues, dim);
    printf("S_AB = %g\n", S_AB);
    S2_AB = calculate_Renyi_entropy(rho_AB_eigenvalues, dim, 2.0);
    printf("S2_AB = %g\n", S2_AB);
    Smin_AB = calculate_min_entropy(rho_AB_eigenvalues, dim);
    printf("Smin_AB = %g\n", Smin_AB);

    diagonalize_symmetric(rho_A, 2, rho_A_eigenvalues);

    printf("\n%g %g\n", rho_A_eigenvalues[0], rho_A_eigenvalues[1]);

    S_A = calculate_vonNeumann_entropy(rho_A_eigenvalues, 2);
    printf("S_A = %g\n", S_A);
    S2_A = calculate_Renyi_entropy(rho_A_eigenvalues, 2, 2.0);
    printf("S2_A = %g\n", S2_A);
    Smin_A = calculate_min_entropy(rho_A_eigenvalues, 2);
    printf("Smin_A = %g\n", Smin_A);

    open_database();

    for(step = 0, t = starttime; t <= stoptime; step++, t = starttime + step * dt)
    {
        printf("=====================================\nt = %g\n", t);
        if(find_results())
        {
            printf("Skipping.\n");
            continue;
        }
        snprintf(buf, sizeof(buf), "J(%g %g %g) β%g t%g ", J_x, J_y, J_z, beta, t);


#ifdef HAAR_RANDOM_UNITARY

        start = clock();
        report_status("Calculating |U(t)>");

        memset(rotated_eigenvectors, 0, dim * dim * sizeof(double _Complex));


        // get_Haar_random_unitary(&random_unitary[5], 2);
        // get_Haar_random_unitary(&random_unitary[12], 2);

        // //  0  1  2  3
        // //  4  5  6  7
        // //  8  9 10 11
        // // 12 13 14 15

        // random_unitary[9] = random_unitary[7];
        // random_unitary[10] = random_unitary[8];
        
        // random_unitary[0] = random_unitary[12];
        // random_unitary[3] = random_unitary[13];
        // random_unitary[12] = random_unitary[14];

        // random_unitary[7] = random_unitary[8] = random_unitary[13] = random_unitary[14] = 0.0;


        get_Haar_random_unitary(random_unitary, 4);

        check_unitary(random_unitary, 4);

        for(i = 0; i < dim; i++)
        {
            for(j = 0; j < dim; j++)
            {
                for(k = 0; k < dim; k++)
                {
                    rotated_eigenvectors[i + dim * j] += random_unitary[i + dim * k] 
                        * hamiltonian[k + dim * j];
                }
            }
        }

        memset(tfd_state, 0, dim * dim * sizeof(double _Complex));
        for(k = 0; k < dim; k++)
        {
            for(i = 0; i < dim; i++)
            {
                for(j = 0; j < dim; j++)
                {
                    tfd_state[i + dim * j] +=
                        exp(-0.5 * beta * eigenvalues[k])
                        * cexp(-1.0 * _Complex_I * t * eigenvalues[k])
                        * hamiltonian[i + dim * k]
                        * rotated_eigenvectors[j + dim * k];
                }
            }
        }
        report_elapsed_time();

#else

        memset(tfd_state, 0, dim * dim * sizeof(double _Complex));
        for(k = 0; k < dim; k++)
        {
            for(i = 0; i < dim; i++)
            {
                for(j = 0; j < dim; j++)
                {
                    tfd_state[i + dim * j] +=
                        exp(-0.5 * beta * eigenvalues[k])
                        * cexp(-1.0 * _Complex_I * t * eigenvalues[k])
                        * hamiltonian[i + dim * k]
                        * hamiltonian[j + dim * k];
                }
            }
        }
#endif

        memset(rho_AX, 0, dim * dim * sizeof(double _Complex));
        for(A_row = 0; A_row < 2; A_row++)
        {
            for(A_column = 0; A_column < 2; A_column++)
            {
                for(B_diag = 0; B_diag < 2; B_diag++)
                {
                    for(C_row = 0; C_row < 2; C_row++)
                    {
                        for(C_column = 0; C_column < 2; C_column++)
                        {
                            for(D_diag = 0; D_diag < 2; D_diag++)
                            {
                                int idx = A_row + ((C_row + ((A_column + (C_column << 1)) << 1)) << 1);
                                double _Complex bra = conj(tfd_state[A_row + ((B_diag + ((C_row + (D_diag << 1)) << 1)) << 1)]);
                                double _Complex ket = tfd_state[A_column + ((B_diag + ((C_column + (D_diag << 1)) << 1)) << 1)];
                                rho_AX[idx] += ket * bra / partitionSum;
                            }
                        }
                    }
                }
            }
        }

#ifdef DEBUG
        check_hermitian(rho_AX, dim);
#endif
        report_status("Diagonalizing rho_AX");
        diagonalize_hermitian(rho_AX, dim, rho_AX_eigenvalues);

        S_AC = calculate_vonNeumann_entropy(rho_AX_eigenvalues, dim);
        printf("S_AC = %g\n", S_AC);
        S2_AC = calculate_Renyi_entropy(rho_AX_eigenvalues, dim, 2.0);
        printf("S2_AC = %g\n", S2_AC);
        Smin_AC = calculate_min_entropy(rho_AX_eigenvalues, dim);
        printf("Smin_AC = %g\n", Smin_AC);


        memset(rho_AX, 0, dim * dim * sizeof(double _Complex));
        for(A_row = 0; A_row < 2; A_row++)
        {
            for(A_column = 0; A_column < 2; A_column++)
            {
                for(B_diag = 0; B_diag < 2; B_diag++)
                {
                    for(C_diag = 0; C_diag < 2; C_diag++)
                    {
                        for(D_row = 0; D_row < 2; D_row++)
                        {
                            for(D_column = 0; D_column < 2; D_column++)
                            {
                                int idx = A_row + ((D_row + ((A_column + (D_column << 1)) << 1)) << 1);
                                double _Complex bra = conj(tfd_state[A_row + ((B_diag + ((C_diag + (D_row << 1)) << 1)) << 1)]);
                                double _Complex ket = tfd_state[A_column + ((B_diag + ((C_diag + (D_column << 1)) << 1)) << 1)];
                                rho_AX[idx] += ket * bra / partitionSum;
                            }
                        }
                    }
                }
            }
        }

#ifdef DEBUG
        check_hermitian(rho_AX, dim);
#endif
        report_status("Diagonalizing rho_AX");
        diagonalize_hermitian(rho_AX, dim, rho_AX_eigenvalues);

        S_AD = calculate_vonNeumann_entropy(rho_AX_eigenvalues, dim);
        printf("S_AD = %g\n", S_AD);
        S2_AD = calculate_Renyi_entropy(rho_AX_eigenvalues, dim, 2.0);
        printf("S2_AD = %g\n", S2_AD);
        Smin_AD = calculate_min_entropy(rho_AX_eigenvalues, dim);
        printf("Smin_AD = %g\n", Smin_AD);

        I3 = 4 * S_A - (S_AB + S_AC + S_AD);
        I3_2 = 4 * S2_A - (S2_AB + S2_AC + S2_AD);
        I3_min = 4 * Smin_A - (Smin_AB + Smin_AC + Smin_AD);

        printf("I3 = %g\n", I3);
        printf("I3_2 = %g\n", I3_2);
        printf("I3_min = %g\n", I3_min);

        store_results();

    }

    close_database();

    free(rho_AX_eigenvalues);
    free(rho_AX);
    free(rho_A_eigenvalues);
    free(rho_A);
    free(rho_AB_eigenvalues);
    free(rho_AB);
    free(tfd_state);
    free(eigenvalues);
    free(hamiltonian);
    
    start = init;
    report_elapsed_time();
    return 0;
}

void open_database(void)
{
    rc = sqlite3_open(DBPATH, &db);
    if(rc) {
        fprintf(stderr, "\n100: Can't open database: %s\n", sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_busy_timeout(db, 100000);

    rc = sqlite3_exec(db, 
        "CREATE TABLE IF NOT EXISTS " TABLENAME " "
        "(J_x real, J_y real, J_z real, "
        "B real, beta real, t real, "
        "S_AB real, S_A real, S_AC real, S_AD real, I3 real, "
        "S2_AB real, S2_A real, S2_AC real, S2_AD real, I3_2 real, "
        "Smin_AB real, Smin_A real, Smin_AC real, Smin_AD real, I3_min real, "
        "CONSTRAINT results_uniq UNIQUE "
        "(J_x, J_y, J_z, B, beta, t))", NULL, 0, &zErrMsg);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n101: SQL error %i: %s\n", rc, zErrMsg);
        sqlite3_free(zErrMsg);
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_prepare_v2(db, 
        "INSERT INTO " TABLENAME " (J_x, J_y, J_z, B, beta, t, "
        "S_AB, S_A, S_AC, S_AD, I3, "
        "S2_AB, S2_A, S2_AC, S2_AD, I3_2, "
        "Smin_AB, Smin_A, Smin_AC, Smin_AD, I3_min) "
        "VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14, ?15, ?16, ?17, ?18, ?19, ?20, ?21);", 
        -1, &stmt_insert, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n102: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM " TABLENAME " WHERE "
        "J_x = ?1 AND J_y = ?2 AND J_z = ?3 AND B = ?4 AND beta = ?5 AND t = ?6;", -1, &stmt_select, NULL);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n103: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

}




int find_results(void)
{
    int count;

    rc = sqlite3_bind_double(stmt_select, 1, J_x);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n200: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_select, 2, J_y);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n201: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_select, 3, J_z);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n202: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_select, 4, B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n203: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_select, 5, beta);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n204: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_select, 6, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n205: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt_select);
    if(rc != SQLITE_ROW)
    {
        fprintf(stderr, "\n206: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
    
    count = sqlite3_column_int(stmt_select, 0);

    rc = sqlite3_reset(stmt_select);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\n207: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
    return count > 0;
}

void store_results(void)
{
    
    rc = sqlite3_bind_double(stmt_insert, 1, J_x);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n300: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 2, J_y);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n301 SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 3, J_z);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n302: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 4, B);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n303: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 5, beta);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n304: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 6, t);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n305: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 7, S_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n306: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 8, S_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n307: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 9, S_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n308: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 10, S_AD);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n309: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 11, I3);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n310: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 12, S2_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n311: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 13, S2_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n312: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 14, S2_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n313: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 15, S2_AD);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n314: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 16, I3_2);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n315: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 17, Smin_AB);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n316: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 18, Smin_A);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n317: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 19, Smin_AC);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n318: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 20, Smin_AD);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n319: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_bind_double(stmt_insert, 21, I3_min);
    if(rc != SQLITE_OK)
    {
        fprintf(stderr, "\n320: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_step(stmt_insert);
    if(rc != SQLITE_OK && rc != SQLITE_DONE) {
        fprintf(stderr, "\n321: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        if(rc == SQLITE_CONSTRAINT)
        {
            return;
        }
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_reset(stmt_insert);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\n322: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
}

void close_database(void)
{
    rc = sqlite3_finalize(stmt_select);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\n401: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_finalize(stmt_insert);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\n402: SQL error %i: %s\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }

    rc = sqlite3_close(db);
    if(rc != SQLITE_OK) {
        fprintf(stderr, "\n403: SQL error %i: %s\n\n", rc, sqlite3_errmsg(db));
        exit(EX_SOFTWARE);
    }
}
