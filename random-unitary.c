#include "random-unitary.h"
#include "util.h"



void get_normal_samples(double _Complex *M, int N);

void get_normal_samples(double _Complex *M, int N)
{
    int i;

    double x,y,rsq,f;

    for (i = 0; i < N; i++)
    {
        do {
            x = 2.0 * arc4random() / (double) UINT32_MAX - 1.0;
            y = 2.0 * arc4random() / (double) UINT32_MAX - 1.0;
            rsq = x * x + y * y;
        } while( rsq >= 1.0 || rsq == 0.0 );
        f = sqrt( -1.0 * log(rsq) / rsq );
        M[i] = x * f + y * f * _Complex_I;
    }

}

void get_Haar_random_unitary(double _Complex *U, int N)
{
	int ret, lwork;
	int i,j;
	double _Complex workopt;
	double _Complex *tau;
	double _Complex *work;
	double *diagonal;

    if(N < 1)
    {
        return;
    }

    tau = calloc(N, sizeof(double _Complex));
    diagonal = calloc(N, sizeof(double));
    
    if(!tau || !diagonal)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

	get_normal_samples(U, N * N);

	lwork = -1;
	zgeqrf_(&N, &N, U, &N, tau, &workopt, &lwork, &ret);

	if(ret != 0)
    {
        fprintf(stderr, "ERROR ZGEQRF failed.\n");
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;
    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zgeqrf_(&N, &N, U, &N, tau, work, &lwork, &ret);

    if(ret != 0)
    {
        fprintf(stderr, "ERROR ZGEQRF failed.\n");
        exit(EX_SOFTWARE);
    }

    free(work);

    for(i = 0; i < N; i++)
    {
    	diagonal[i] = creal(U[i + i * N])/fabs(creal(U[i + i * N]));
    }

	lwork = -1;
	zungqr_(&N, &N, &N, U, &N, tau, &workopt, &lwork, &ret);

	if(ret != 0)
    {
        fprintf(stderr, "ERROR ZUNGQR failed.\n");
        exit(EX_SOFTWARE);
    }

    lwork = (int) workopt;
    work = malloc(lwork * sizeof(double _Complex));

    if(!work)
    {
        fprintf(stderr, "Could not allocate memory.");
        exit(EX_OSERR);
    }

    zungqr_(&N, &N, &N, U, &N, tau, work, &lwork, &ret);


    if(ret != 0)
    {
        fprintf(stderr, "ERROR ZUNGQR failed.\n");
        exit(EX_SOFTWARE);
    }

    for(i = 0; i < N; i++)
    {
        for(j = 0; j < N; j++)
        {
            U[i + j * N] *= diagonal[i];
        }
    }
    
    free(work);
    free(diagonal);
    free(tau);
}

void get_diagonal_unitary(double _Complex *U, int N)
{
    int i;
    double _Complex *work;

    work = calloc(1, sizeof(double _Complex));

    memset(U, 0, sizeof(double _Complex) * N * N);

    for(i = 0; i < N; i++)
    {
        get_Haar_random_unitary(work, 1);
        U[i + i * N] = work[0];
    }
    free(work);
}

void get_identity_unitary(double _Complex *U, int N)
{
    int i;
    memset(U, 0, sizeof(double _Complex) * N * N);

    for(i = 0; i < N; i++)
    {
        U[i + i * N] = 1.0;
    }
}
